IFJ
=======

What is it
-------

ifj is a POSIX shell script that inject init diversity in a single package.
It is mainly designed for Devuan GNU+Linux.

Documentation
-------

The script read a Debian/Devuan package, inject init script/config and create 
new package.

ifj assumes that run script/init config resides in:

	/var/local/INITSYSTEM/

and are copied in the package.

Script I wrote support epoch and runit. Other init can be supported if 
implemented.

### Epoch

Config file for epoch are in:

	/var/local/epoch/package_name.conf

for example:

	/var/local/epoch/openssh-server.conf

Multiple objects (daemon) are allowed in the file.

## Runit

Script file for runit are in:

	/var/local/runit/package/

daemon name are in subdirectories, for example:

	/var/local/runit/openssh-server/sshd/run

System one time tasks are in:

	/var/local/runit/package/nodaemon/{1,2,3}

and are injected in:

	/etc/runit/{1,2,3}

Bugs
-------
No support for runit priority in system one time tasks.
Workaround for postinst and prerm fail if script exit with "exit 0" or similar 
at bottom.

License
-------

ifj is released with GNU Affero Public License version 3
([AGPLv3](http://www.gnu.org/licenses/agpl-3.0.html)).

Contacts 
-------

Viverna <viverna@inventati.org>
